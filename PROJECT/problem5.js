function problem5(inventory,allCarYear){
    const allOldCarYears=[];
    for (let i=0;i<allCarYear.length;i++){
        if (allCarYear[i]<2000){
            let oldCarYear=allCarYear[i];
            allOldCarYears.push(oldCarYear);
        }
    }
    return allOldCarYears;
}
module.exports=problem5;