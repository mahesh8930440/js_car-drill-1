function problem4(inventory){
    const allCarYears=[];
    for (let i=0;i<inventory.length;i++){
        let carYear=inventory[i].car_year;
        allCarYears.push(carYear);
    }
    return allCarYears;
}
module.exports=problem4;