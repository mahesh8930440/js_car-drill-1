function problem2(inventory) {
    const lastCarIndex=inventory.length-1
    const lastCar=inventory[lastCarIndex];
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
        
}

module.exports = problem2;