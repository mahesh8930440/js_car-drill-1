function problem3(inventory){
    const allCarModels=[];
    
    for (let i=0 ;i < inventory.length;i++){
        let carModel=inventory[i].car_model;
        allCarModels.push(carModel);
    }

    for (let i=0 ;i<allCarModels.length;i++){
        for (let j=0 ;j<allCarModels.length-1;j++){
            if (allCarModels[j].toLowerCase() > allCarModels[j+1].toLowerCase()){
                let swapElement=allCarModels[j];
                allCarModels[j]=allCarModels[j+1];
                allCarModels[j+1]=swapElement;
            }
        }
    }
    
   return allCarModels;
}
module.exports=problem3;